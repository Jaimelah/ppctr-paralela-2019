# P1: Multithreading en C++
Autor: Jaime López-Agudo Higuera
Fecha: 11-12-2019

##PREFACIO
La práctica ha sido bastante extensa, especialmente la parte de los benchamrks y la generación de
gráficas, la forma de crear el informe no me ha parecido la más optima, hubiese preferido
entregar un pdf creado en LaTex, por ejemplo.

##Índice

1. Sistema
2. Diseño e implementación
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

##1. Sistema
El sistema sobre el que se han ejecutado los benchamarks es un portatil conectado a corriente alterna(para evitar posibles ahorros de energía), las características
del sistema son las sigientes:
- Host
  - CPU AMD Ryzen 5 3550H with Radeon Vega Mobile G
  - CPU 8 cores: 1 socket, 2 threads/core, 4 Cores
  - CPU @2100Mhz (fijada con cpufreq en modo usuario).
  - CPU cache L1d: 128KiB, L1i: 256KiB, L2: 2MiB, L3: 4MiB
  - 8GiB RAM DDR4 3200Mhz
  - 80 procesos en promedio con cada ejecución.
  - Distribución Ubuntu 19.10 actalizada.
  - CPU flags:sse, sse2, sse3, sse4a, sse4_1, sse4_2, avx...
  - Ejecutado sobre HDD 5500rpm 1 TB





2.-Diseño e implementación del software

2.1-Parte Común
La parte común de la práctica consiste en leer los parametros que se pasan desde consola de forma que se recciba el número de operaciones
a realizar, el tipo de operación que se tiene que realizar(SUM, SUB, XOR).
Esta parte también se encarga de inicializar las variables que serán usadas en los dos tipo de ejecución del problema (threads-logger) o (atomico-logger).
En esta parte, también comenzaremos el tiempo que se encargará de marcar el tiempo de ejecución
de la tarea secuencial o de la tarea paralela, esto lo iniciamos al finalizar la parte común ya que el ROI comienza a partir de este momento.
De esta manera podremos comparar de forma más real el tiempo de las ejecuciones paralelas con el de las secuenciales.


2.2.-Parte SECUENCIAL
La parte secuencial comienza al determinar que se nos está pasando por el interprete de comandos una ejecución secuencial, es decir,
el número de argumentos pasados es 3 (argc==3), una vez habiendo determinado esto, necesitamos ejecutar una función secuencial que se encargue
de los cálculos del interior del array de Doubles sobre el que necesitamos operar, esta función tiene la siguiente forma:
sequential(double *array, int numOperaciones, double *resultado).
Esta función consiste en un switch(g_tipo_operacion), que segun sea SUM, SUB o XOR realice un bucle calculando el resultado de forma secuencial.  (*)




2.3.-Parte paralela
La parte paralela de la aplicación comienza al determinar si los argumentos pasados por el interprete de comandos son para una ejecución paralela,
es decir, si el número de argumentos es 5 (argc==5),al hacer esto necesitamos recoger el número de threads
que se le pasa desde el intérprete de comandos, esto nos servirá para delimitar la cantidad
de operaciones que tiene que realizar cada uno de los threads.
Esta parte de la aplicación se encarga de generar las estructuras de los threads, stuct thread_data, (definido ya en la aprte común).

Esta parte genera los threads mediante la API de C++, los threads son creados sobre un array de threads mediante el uso de la llamada
std::thread(parallel,thread_data, thread_data(resto),&resultado);
La funcion worker del thread (parallel) se encarga del cálculo de los valores de cada uno de las partes del array correspondiente a cada thread.
Esta parte también esta encargada de determinar si hay operaciones restantes al finalizar las operaciones cada uno de los threads,
esta seccion (resto), será operada por el primer thread que acabe su trabajo principal.

Los threads suman sus valores sobre un resultado que se pasa como parámetro a la función worker del thread de forma que necesitaremos
proteger la escritura sobre esta variable, para evitar posibles problemas de concurrencia sobre &resultado.
Para esto usaremos la función de la API pthread de C++ que nos proporciona mutex, con lo que podremos realizar la preotección del
segemento de codigo en el que se realiza la suma sobre la variable resultado.

Al acabar los threads de operar, se necesitará hacer join() de cada thread para terminar la ejecución de cada uno de los threads, además de marcar el final
del tiempo de ejecución que iniciamos con anterioridad en la aprte básica de la práctica

2.4.-Parte Logger
La parte del FEATURE_LOGGER se encarga de hacer el calculo de los valores de los threads de manera
independiente de forma que al final de la ejecución se comparen los resultados del logger y de los threads para comprobar que coinciden.
Para ello necesitaremos crear un thread extra (logger), al que se pase la worker function  opLogger, que se encargue de almacenar
los valores pasados por los threads además de operarlos para obtener el resultado final que se comparará en el main para determinar si son iguales.
Para esto, cada thread mandará un notify_one cada vez que tenga el resultado para añadir al array.
El thread logger ira determinando que threads han mandado una notificación mediante el uso de un contador que contenga
el número de threads que han hecho el notify y se irá sumando los resultados del array de resultados parciales de los threads,
hasta que llegue el momento que el contador de threads que han pedido notify() pase a ser 0, y por lo tanto se acaben los resultados, y el logger tenga que hacer join() para acabar su thread independiente.

2.5.-Parte Optimize
La parte del optimize consiste en el uso de variables atómicas para optimizar las ejecuciones de la parte paralela y
comparar los resultados con la parte del Logger.
Para implementar esta parte hemos la librería atomic, para poder crear una variable atómica resultado, que ha de ser operada de igual manera que lo realizado
con anterioridad en la parte paralela

2.6.-DEBUGGER
Se ha implementado un debugger que puede ser lanzado mediante una variable de preprocesador que se encargará de mostrar
prints en lugares importantes de código de manera que en caso de erro se pueda seguir la ejecución para poder depurarla.

2.7.-JAVA-BRIDGE
Se ha implementado la parte del bridge con java, para lo que se ha copiado el código de la parte paralela de c++.
Al necesitar una libreria externa libp1bridge.so, he introducido esta dentro del include de la carpeta jdk, para evitar tener que ejecutar el benchmark teniendo que linkar cada vez esta librería.

3.-Metodología y desarrollo de las pruebas realizadas
Para los benchmarks se han realizado 11 pruebas en 2 estaciones distintas con 3 cantidades de operaciones y 6 cantidades de threads (1,2,4,6,8,10)
De estas 11 ejecuciones se ha eliminado siempre la primera que sirve como warmup, para realizar las ejecuciones de los benchamarks
se ha comenzado fijando la frecuencia de la CPU a 2100Mhz, además de eliminando todo posible proceso que se haya quedado en el background que en algún
momento de la ejecución haya podido generar un pico inesperado de tiempo, como pasa en algunos casos, mostrados en la tabla excell incluida en la entrega.


Los benchmarks usados estan adjuntados en la práctica y son de la isguiente forma:
(benchmark.sh)
#!/usr/bin/env bash
file=resultsO10.log
touch $file
for season in 1 2; do
    for benchcase in st1 mt1 st2 mt2 st3 mt3; do
        echo $benchcase >> $file
        for i in `seq 1 1 11`;
        do
            ./benchsuite.sh $benchcase >> $file # results dumped
            sleep 1
        done
    done
done
(benchsuite.sh)
#!/usr/bin/env bash
size=100000
size2=1000000
size3=40000000
op=SUM
nthreads=10
case "$1" in
    st1)
        ./p1 $size $op
        ;;
    mt1)
        ./p1 $size $op --multi-thread $nthreads
        ;;
    st2)
        ./p1 $size2 $op
        ;;
    mt2)
        ./p1 $size2 $op --multi-thread $nthreads
        ;;
    st3)
        ./p1 $size3 $op
        ;;
    mt3)
        ./p1 $size3 $op --multi-thread $nthreads
esac

En el caso del segundo benchamrk, el número de threads se ha ido cambiando manualmente para generar los archivos creados en la carpeta resultaadosBenchmarks.
En esta carpeta, la sitaxis es la siguiente: results<nThreads>.log contiene infrmación secuencial y paralela sin optimize con ese número de threads.
y resultsO<nThreads>.log: contiene información de la parte secuencial además de ejecuciones optimized con nThreads.


Se ha adjuntado a su vez una hoja excel con la información de speedups y medias de tiempos de ejecución de los diversos tipos.
GRAFICAS:

<img src="resultadosBenchmarks/B1.png" width="500" style="display:block; margin:0 auto;"/>

En cuanto a los resultados obtenidos, observamos que es speedUp con los 40000000 operaciones es un claro x2 en el caso de 1Thread vs 2Threads, 2Threads vs 4Threads y asi sucesivamente,
hasta qu ellegamos a los 8 threads, que es donde esto deja de cumplirse, ya que en el caso de mi ordenador, el número máximo de nucleos me limita por esa parte.
Tabien observamos que la parte secuencial es más rápida que la parte paralela con 1 thread, esto es debido a que introducimos un overhead extra al crear y gestionar los threads de la
aplicación, esto tambien lo bservamos con la ejecuciones de la aprte cn el optimize vs la part eparalela completa, ya que al relaizar la suma sobre los atómicos estamos introduciendo
overhead.

Por lo que obetnemos el mejor rendimiento con 8 cores (mi caso), y obtenemos el peor rendimiento con optimize de 1 solo core, debido a la gran cantidad de overheads generados por la gestión

Por lo que podemos concluir que para obtener el mayor rendimiento no solo basta con ajustar el código que se va a ejecutar, sino que también debemos ajustarnos a la maquina sobre la que
se va a ejecutar dicho código, ya que por ejemplo, si hubesemos realizado el benchmarking sobre una maquina con 12 cores 24 threads, la ejecución con 10 hilos seguiría manteniendo una mejora
tangible en los tiempos de ejecución, no como ocurre en mi caso, que es al contrario, ya que estos tiempos se engrosan.
