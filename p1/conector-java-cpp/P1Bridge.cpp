#include <jni.h>
#include "P1Bridge.h"

#include <iostream>
#include <cstdlib>
#include <thread>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <mutex>
#include <condition_variable>
#include <atomic>
//Variables de preprocesador
#define FEATURE_LOGGER 1
#define DEBUG 0
#define FEATURE_OPTIMIZE 1
//Variables para el FEATURE_LOGGER
#if FEATURE_LOGGER
  std::condition_variable cv;
  bool ready=false;
  double *logPointer;
  int threadsPidenLogger=0;
#endif
//Variables para FEATURE_OPTIMIZE(atomicos)
#if FEATURE_OPTIMIZE
  double resulOptimized=0;
  std::atomic<double> result_atomic(resulOptimized);
#endif

//Struct con la información pasada a los threads
typedef struct{
  int inicio;
  int numOperaciones;
  double *puntero_array;
  int idThread;
}thread_data;
//Variables globales
int g_tipoOperacion=-1;
char* g_charOperacion;
int g_primerThread=-1;
std::mutex g_semaforo;
//Prototipos de funciones
void sequential(double* , int , double*);
void parallel(thread_data* ,thread_data*, double*);
double opParallel(thread_data*);
void opLogger(int, double *, std::condition_variable *,bool *);
struct timeval start,end;
double execTime=0;
double resultadoComun=0;

/**
  * Practica 1: parte secuencial que se encarga de operar el array de datos pasados
  * las operaciones aceptadas son SUM(suma),SUB(resta) y XOR(XOR)
  * Secuencial: ./p1 <número de operaciones> <tipo de operación> --single-thread
  *
  * @author Jaime López-Agudo Higuera
  * @date 9-11-2019
  * @param array(double*), numOperaciones(int), resultado(double*)
  */
void sequential(double *array,int numOperaciones,double *resultado){
  for(int i=0;i<numOperaciones;i++){
    switch(g_tipoOperacion){
      case 0:
        *resultado=*resultado+array[i];
        break;
      case 1:
        *resultado=*resultado-array[i];
        break;
      case 2:
        *resultado=(int)*resultado^(int)array[i];
        break;
      default:
        printf("Error parte secuencial\n");
        break;
    }
  }
}
/**
  * Practica 1: parte paralela que recibe la información dentro del struct y manda a una función externa
  * operar lo que necesite en función de la operacion guardada
  * Paralelo: ./p1 <número de operaciones> <tiipo de operación> --multi-thread <número de threads>
  *
  * @author Jaime López-Agudo Higuera
  * @date 9-11-2019
  * @param threadOperado(thread_data*), resto(thread_data*),resultado(double*)
  */
void parallel(thread_data* threadOperado, thread_data* resto, double *resultado){
  double resultadoParcial=opParallel(threadOperado);
  std::lock_guard<std::mutex>guard(g_semaforo);
  //DEtecto si necesitamos el thread extra para acabar las operaciones:
  if(g_primerThread==-1 &&resto->numOperaciones!=0){
    g_primerThread=threadOperado->idThread;
    if(g_tipoOperacion!=2){
      resultadoParcial=resultadoParcial+opParallel(resto);
    }else{
      resultadoParcial=(int)resultadoParcial^(int)opParallel(resto);
    }

  }
  #if DEBUG
    printf("Soy el thread %d y he calculado: %f\n",threadOperado->idThread,resultadoParcial);
  #endif
  #if FEATURE_OPTIMIZE
    switch(g_tipoOperacion){
      case 0:
        result_atomic=result_atomic+resultadoParcial;
      break;
      case 1:
        result_atomic=result_atomic+resultadoParcial;
      break;
      case 2:
        result_atomic=(int)result_atomic^(int)resultadoParcial;
      break;
      default:
        printf("Error en paralelo optimize\n");
      break;
    }
  #else
    switch(g_tipoOperacion){
      case 0:
        *resultado=*resultado+resultadoParcial;
      break;
      case 1:
        *resultado=*resultado+resultadoParcial;
      break;
      case 2:
        *resultado=(int)*resultado^(int)resultadoParcial;
      break;
      default:
        printf("Error en paralelo\n");
      break;
    }
  #endif
  #if FEATURE_LOGGER
    logPointer[threadOperado->idThread]=resultadoParcial;
    threadsPidenLogger++;
    ready=true;
    cv.notify_one();
  #endif
}
#if FEATURE_LOGGER
/**
  * Practica 1: parte logger que se encarga de operar paralelamente mediante otro hilo el resultado de
  * las operaciones de los threads.
  *
  * @author Jaime López-Agudo Higuera
  * @date 9-11-2019
  * @param nThreads(int), resultadoLogger(double*), cvLog(*condition_variable),
  */
  void opLogger(int nThreads, double *resultadoLogger, std::condition_variable *cvLog, bool *fin){
    double tmp=0;
    double resultados[nThreads];
    while(threadsPidenLogger!=nThreads){
      std::unique_lock<std::mutex>l(g_semaforo);
      cv.wait(l,[]{return ready;});
      ready=false;
      l.unlock();
    }
    for(int i=0;i<nThreads;i++){
      resultados[i]=logPointer[i];
    }
    #if DEBUG
      for(int i=0;i<nThreads;i++){
        printf("LOGGER soy el thread: %d valor: %f\n",i,resultados[i]);
      }
    #endif
      for(int i=0;i<nThreads;i++){
        if(g_tipoOperacion!=2){
          tmp=tmp+resultados[i];
        }else{
          tmp=(int)tmp^(int)resultados[i];
        }
      }

    std::lock_guard<std::mutex>guard(g_semaforo);
    *fin=true;
    *resultadoLogger=tmp;
    printf("Solucion logger %f\n",*resultadoLogger);
    cvLog->notify_one();
  }
#endif
/**
  * Practica 1: parte que se encarga de la operación dentro de cada uno de los threads.
  *
  *
  * @author Jaime López-Agudo Higuera
  * @date 9-11-2019
  * @param thread(*thread_data)
  */
double opParallel(thread_data *thread){
  double tmp;

    switch(g_tipoOperacion){
      case 0:
        for(int i=thread->inicio;i<thread->numOperaciones+thread->inicio;i++){
          tmp=tmp+thread->puntero_array[i];
        }
      break;
      case 1:
      for(int i=thread->inicio;i<thread->numOperaciones+thread->inicio;i++){
        tmp=tmp-thread->puntero_array[i];
      }
      break;
      case 2:
      for(int i=thread->inicio;i<thread->numOperaciones+thread->inicio;i++){
        tmp=(int)tmp^(int)thread->puntero_array[i];
      }
      break;
      default:
          printf("Error parte paralela op\n");
          exit(-1);
      break;
    }
    return tmp;
}

int todo (int numOpTotal,char* operation,char* threading,int nThreads) {
  //Lectura de número de threads
  if(nThreads>12||nThreads<1){
    printf("Numero de threads erroneo tienen que ser entre 1 y 12\n");
    exit(-1);
  }
  g_charOperacion=operation;
  if(strcmp(operation,"SUM")==0){
    g_tipoOperacion=0;
  }else if(strcmp(operation,"SUB")==0){
    g_tipoOperacion=1;
  }else if(strcmp(operation, "XOR")==0){
    g_tipoOperacion=2;
  }else{
    printf("Error de operacion\n");
  }
  #if DEBUG
      printf("argv[4]: %i\n",nThreads);
  #endif
  //reservas de memoria
  double *array=(double*)malloc(numOpTotal*sizeof(double));
  thread_data infoThreads[nThreads];
  int operacionesThread=numOpTotal/nThreads;
  int operacionesRestantes=numOpTotal%nThreads;
  //Relleno el array
  for(int i =0; i<numOpTotal;i++){
    array[i]=(double)i;
    #if DEBUG
      printf("array[%i] %f\n",i,array[i]);
    #endif
  }
  //Struct operado si hay resto
  thread_data resto;
  resto.numOperaciones=operacionesRestantes;
  if(operacionesRestantes!=0){
    resto.inicio=numOpTotal-operacionesRestantes;
    resto.puntero_array=array;
  }
  //Structs de los threads normales
  for(auto i =0;i<nThreads;i++){
    infoThreads[i].inicio=i*operacionesThread;
    infoThreads[i].idThread=i;
    infoThreads[i].puntero_array=array;
    infoThreads[i].numOperaciones=operacionesThread;
  }
  //Inicio tiempo(paralelo)
  gettimeofday(&start,NULL);
  //Preparacion del hilo Logger
  #if FEATURE_LOGGER
    //Variable condicional para logger
    std::condition_variable cvLogger;
    std::thread logger;
    bool finLogger=false;
    double resultadoLogger=0;
    double arrayLogger[nThreads];
    logPointer=arrayLogger;
    logger=std::thread(opLogger,nThreads,&resultadoLogger,&cvLogger,&finLogger);
  #endif
  //Inicio threads
  std::thread listaThreads[nThreads];
  for(auto i=0;i<nThreads;i++){
      listaThreads[i]=std::thread(parallel,&infoThreads[i],&resto,&resultadoComun);
  }
  //fin threads
  for(auto i=0;i<nThreads;i++){
    listaThreads[i].join();
  }
  //fin tiempo
  gettimeofday(&end,NULL);
  //fin del logger
  #if FEATURE_LOGGER
    std::unique_lock<std::mutex>l(g_semaforo);
    cvLogger.wait(l,[&]{return finLogger;});
    l.unlock();
    logger.join();
    //decidir que resultado se compara (atómico-logger o threads-logger)
    #if FEATURE_OPTIMIZE
      std::atomic<double> *puntero=&result_atomic;
    #else
      double *puntero=&resultadoComun;
    #endif
    //Comparacion de resultados
    if(resultadoLogger==*puntero){
      printf("soluciones coinciden\n");
    }else{
      printf("Soluciones no coinciden\n");
      return 1;
    }
  #endif
  //Libero espacio ocupado por el array con los datos
  free(array);
  //Saco tiempo de ejecucion en milisegundos
  execTime=(end.tv_sec-start.tv_sec)*1000+(end.tv_usec-start.tv_usec)/1000.0;

  #if FEATURE_OPTIMIZE
    printf("Resultado con atomico %f, tiempoExec: %f ms, numOperaciones: %d, tipoOperacion: %s, numThreads %d\n",result_atomic.load(),execTime,numOpTotal,operation,nThreads);
  #else
    printf("Paralelo: resultado %f, ExexcTime %f ms, nOperaciones: %d, tipoOperacion %s \n",resultadoComun,execTime,numOpTotal,operation);
  #endif
  return resultadoLogger;
}


JNIEXPORT jint JNICALL Java_P1Bridge_compute(JNIEnv *env, jobject thisObj,int length,jstring g_tipoOperacion,jstring threading,int nThreads) {
  const char *nativeString = env->GetStringUTFChars(g_tipoOperacion, 0);
	char* operation = (char*)nativeString;
	nativeString = env->GetStringUTFChars(threading, 0);
	char* multiT = (char*)nativeString;
  todo(length,operation,multiT,nThreads);
}
