#!/usr/bin/env bash
file=resultsO10.log
touch $file
for season in 1 2; do
    for benchcase in st1 mt1 st2 mt2 st3 mt3; do
        echo $benchcase >> $file
        for i in `seq 1 1 11`;
        do
            ./benchsuite.sh $benchcase >> $file # results dumped
            sleep 1
        done
    done
done
