#!/usr/bin/env bash
size=100000
size2=1000000
size3=40000000
op=SUM
nthreads=10
case "$1" in
    st1)
        ./p1 $size $op
        ;;
    mt1)
        ./p1 $size $op --multi-thread $nthreads
        ;;
    st2)
        ./p1 $size2 $op
        ;;
    mt2)
        ./p1 $size2 $op --multi-thread $nthreads
        ;;
    st3)
        ./p1 $size3 $op
        ;;
    mt3)
        ./p1 $size3 $op --multi-thread $nthreads
esac
