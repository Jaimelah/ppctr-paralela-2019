# P2: Multithreading en C++
Autor: Jaime López-Agudo Higuera

Fecha: 18-12-2019

# PREFACIO
La práctica 2 consiste en la implementación de un algoritmo de generación de áreas de elipses de 3 formas distintas, una de forma secuencial, otra de forma paralela usando las librerías de threads que nos proporciona el lenguaje c++ y una implementación mediante la API de openMP.

# Índice

1. Sistema
2. Diseño e implementación
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

# 1. Sistema
El sistema sobre el que se han ejecutado los benchamarks es un portátil conectado a corriente alterna(para evitar posibles ahorros de energía), las características
del sistema son las siguientes:
- Host
  - CPU AMD Ryzen 5 3550H with Radeon Vega Mobile G
  - CPU 8 cores: 1 socket, 2 threads/core, 4 Cores
  - CPU @2100Mhz (fijada con cpupower en modo usuario).
  - CPU cache L1d: 128KiB, L1i: 256KiB, L2: 2MiB, L3: 4MiB
  - 8GiB RAM DDR4 3200Mhz
  - Distribución Ubuntu 19.10 actualizada.
  - CPU flags:sse, sse2, sse3, sse4a, sse4_1, sse4_2, avx...
  - Ejecutado sobre HDD 5500rpm 1 TB
- Benchmarks ejecutados sobre un procesador capado a 2.1GHz de manera manual mediante el uso de los comandos de bash cpupower, usando interfaz gráfica de la terminal y con número de procesos habituales ejecutados con el portátil en idle, es decir, el usuario no ha ejecutado ningún proceso extra durante los benchmarks ni en algún momento anterior a estos.
###### Anotación muy importante, al estar ejecutando sobre una arquitectura ZEN + (ryzen 3550H), no podemos asegurar unos speedups claros y exactos cuando ejecutamos por encima de la cantidad de cores lógicos, ya que esta microarquitectura aplica una gestión de memoria cache de instrucciones, peculiar, en la que esta caché es compartida por los 2 threads del core físico, de tal forma que al ejecutar sobre el máximo número de threads de la máquina nunca vamos a poder asegurar el speedUp correspondiente por definición(8 vs 4 threads, speedup aprox =2). Esta información esta presentada en la siguiente url https://en.wikichip.org/wiki/amd/microarchitectures/zen%2B#Memory_Hierarchy y en específico sobre mi CPU: https://en.wikichip.org/wiki/amd/ryzen_5/3550h podemos ver el mapa del die de la propia CPU y más información importante acerca de la caché y su organización. En esto también influye el infinity fabric que es un tipo de comunicación entre cores propietario de AMD, en el que la velocidad de comunicación entre cores se basa en la frecuencia de la RAM, lo cual puede tener cierta influencia en el performance dentro de los benchmarks 

# 2. Diseño e implementación del software

## 2.1. Parte Común
En esta caso la parte común de la práctica consiste en un programa principal que recibirá distintos argumentos desde la consola en función de la forma de ejecución en la que nos encontramos.

 - Forma secuencial: la forma de pasar parámetros a la forma secuencial es la siguiente: ```./seq [numeroIteraciones]```

- Forma paralela: ```./cpp [numeroIteraciones][nThreads]```, aunque en este caso y en el caso de OMP, este valor también puede ser pasado como variable de entorno (uso de getenv("")).

- Forma OMP: ```./omp[numeroIteraciones][nThreads].```
##### La variable de entorno detectada por el programa es OMP_NUM_THREADS
Esta variable la podemos generar usando el comando de bash: ``` export OMP_NUM_THREADS=X```

## 2.2.-Parte SECUENCIAL(SEQ)
La parte secuencial es proporcionada en la propia practica, esta no ha sido cambiada de ninguna manera, de forma que las ejecuciones de los benchmarks dan valores muy grandes en comparación con los valores esperados con una implementación secuencial a partir de la paralela, es decir, una implementación sin el overhead generado por el paso de parámetros a structs (parse() y el uso de ambos structs de manera redundante).

## 2.3.-Parte PARALELA(CPP)
La parte paralela de esta práctica se basa en la forma de paralelización realizada en la primera práctica, pero con una ligera diferencia, ya que al generar los hilos que se encargan de las suboperaciones, en caso de que tengamos una parte restante de cálculo, esta será asignada de manera estática al último hilo generado, de manera que nos ahorramos algo de tiempo de ejecución con respecto a una implementación que requiera determinar que hilo acaba primero y cómo asignar los valores restantes a calcular.

Por lo que para generar los threads hacemos lo siguiente:
```
for(auto i=0;i<nThreads-1;i++){
  threads[i]=std::thread(operaPi,gap,nThreads,iterations,&resultado,iterations/nThreads*i,0);
}
threads[nThreads-1]=std::thread(operaPi,gap,nThreads,iterations,&resultado,iterations/nThreads*(nThreads-1),opRestantes);
```
En este fragmento de código observamos como asignamos valores de opRestantes=0 a los primero hilos y al último le sumamos las operacionesRestantes a las operaciones que tiene ya de por si cada hilo.

Tras realizar las operaciones cada hilo, estos tendrán que sincronizarse y compartir los resultados sobre una variable que se pasa por referencia para evitar duplicaciones de esta (resul), pero esta variable ha de ser accedida de manera independiente por cada thread para que se puedan sumar los subresultados calculados por cada thread, por lo que esta suma (```*resul=*resul+tmp```) ha de ser protegida por un mutex global (g_semaforo), que sera lockeado al acabar un thread sus operaciones y liberado al haber ya sumado sus resultados obtenidos, como podemos ver en el siguiente fragmento:
```
void operaPi(double gap,int nThreads,long iterations,double *resul, int index,long opRestantes){
  double gap2= gap*gap;
  double tmp=0.0;
  long fin=iterations/nThreads+index+opRestantes;
  for(auto i=index;i<fin;i++){
    tmp = tmp + 4/(1+((i+0.5)*(i+0.5)*gap2));
  }
  std::lock_guard<std::mutex>lock(g_safe);
  {
  *resul=*resul+tmp;
  }
}
```

## 2.4.-Parte OMP(OMP)
En la parte de openMP se nos pide plicar una optimización paralela usando las librerías que nos proporciona openMP ara esta tarea, aunque el único pragma que podemos usar en este caso es el ```#pragma omp parallel [clause]...```
```
void omp(double gap, long iterations, int nThreads){
  double tmp=0;
  double gap2= gap*gap;
  long i;
  #pragma omp parallel num_threads(nThreads) private(i) reduction(+:tmp) shared(gap2,iterations)
  {
    int aux=omp_get_thread_num();
    for(i=i+aux; i<iterations;i=i+nThreads){
      tmp=tmp+4/(1+(i+0.5)*(i+0.5)*gap2);
    }
  }
  tmp=tmp*gap;
  tmp=2*tmp/0.489897949;
  //printf("OMP ellipse: %f \n",tmp);
}
```
En este segmento del código observamos que la paralelización consiste en una región paralela (#pragma omp parallel), en la que tendremos nThreads threads, esta variable es la variable de entorno antes enunciada o el valor pasado por la terminal mencionado en la parte común.

La región paralela tiene como variables compartidas gap2 e iterations, que son leídas por todos los threads, con lo que no será necesario crearlas privadas para protegerlas, también tendremos como variable privada el índice del bucle, ya que será distinto para cada sub-bucle ejecutado por cada thread.
Usaremos también la clausula reduction(+:tmp), la cual se encargará de, al acabar todas las ejecuciones de cada uno de los threads, sumar sobre la variable tmp todos los sub-resultados de cada uno.

Las variables compartidas son variables solo de lectura, en este caso es gap2 (gap*gap) que es un valor constante usado en la resolución del área de la elipse e iterations(numero de iteraciones totales).

Dentro de bucle observamos que tiene una forma no convencional esto es debido a que cada uno de los threads se va a llevar un trozo del bucle, para ello hacemos una llamada a ```omp_get_thread_num()``` lo cual nos devolverá un id del thread, que en este caso será desde [0-nThreads], por lo que podremos usar este número para delimitar cada una de las iteraciones de los threads.

## 2.5.-Elementos extra
A la práctica se le ha añadido un Makefile para la compilación condicional rápida del código, el cual genera los 3 archivos sobre el directorio build, además de generar de manera independiente cada uno de los binarios, se ha incluido una parte que se encarga de generar todos los binarios juntos, para agilizar la compilación:
``` cpp:
	mkdir -p build
	g++ -std=c++17 -pthread -D PARALELO -o build/cpp src/p2.cpp
seq:
	mkdir -p build
	g++ -std=c++17 -pthread -D SECUENCIAL -o build/seq src/p2.cpp
omp:
	mkdir -p build
	g++ -std=c++17 -D OMP -o build/omp src/p2.cpp -fopenmp
all:
	mkdir -p build
	g++ -std=c++17 -D OMP -o build/omp src/p2.cpp -fopenmp
	g++ -std=c++17 -pthread -D SECUENCIAL -o build/seq src/p2.cpp
	g++ -std=c++17 -pthread -D PARALELO -o build/cpp src/p2.cpp
clear:
	rm -rf build
```
# 3.-Metodología y desarrollo de las pruebas realizadas
Como hemos indicado antes las pruebas tomadas sobre el código comienzan fijando las frecuencias de los núcleos a 2.1GHz mediante los siguientes comandos: ```sudo cpupower frequency-set -g userspace ``` para colocar la cpu en modo userspace para poder cambiar el valor de frecuencia y ```sudo cpupower frequency-set -f  2100000```para poder lockear la frecuencia de CPU a 2.1 GHZ (2100000Khz), ya que en el caso de la CPU del portátil solo se puede colocar en alguno de los siguientes steps: 1.4GHz, 1.7GHz o 2.1GHz.

Para la ejecución de los benchmarks se ha usado el script facilitado en el directorio de la práctica que contiene las ejecuciones de cada uno de los tests para la parte cpp, este benchmark puede ser editado para que genere los datos de la parte OMP solo con cambiar las ejecuciones de ``` ./build/cpp ....``` a ```./build/omp .... ```.

Esto también podemos hacerlo desde la parte secuencial, aunque tendremos que realizar solo 2 tests con 45000000 operaciones y 90000000 operaciones de forma que nos quedarían 2 benchcases de las siguiente forma dentro del archivo benchsuite.sh:
```
seq1)
    ./build/seq $size
    ;;
seq2)
    ./build/seq $size2
    ;;
```

Para todas las ejecuciones de los tests de OMP, CPP y SEQ se ha intentado mantener en la medida de lo posible las condiciones de ejecución en todos los casos, por lo que al ejecutarlos, la frecuencia estaba fijada, como henos indicado antes, además el portátil ha estado en todo momento conectado a la corriente para evitar problemas con ahorros de energía.

También, al ejecutar los benchmarks, se ha tenido solamente abierto el terminal gráfico de forma que se ha evitado la mayor parte de procesos ejecutados en paralelo en el procesador, aunque es posible encontrar algún pico de tiempo de ejecución que haya podido colarse debido a algún proceso externo del SO o de otro origen que ha podido tomar la CPU y cambiar en cierta medida los tiempos de ejecución.
![CPP vs SEQ](Graficas/speedUpsSEQPAR.png)

En esta gráfica observamos los speedups de la parte paralela frente a la parte secuencial, en ejecuciones de 45000000(azul) y 90000000(naranja), PAR1-> 1thread, PAR2->2 threads...
Podemos observar un speedup bastante alto frente a la parte secuencial
![OMP vs SEQ](Graficas/speedUpsSEQOMP.png)

En esta gráfica observamos los speedups de la parte OMP frente a la parte secuencial, en ejecuciones de 45000000(azul) y 90000000(naranja), OMP1-> 1thread, OMP2->2threads...
![execTime par](Graficas/tiempoEjecPAR.png)

En esta gráfica podemos ver el tiempo de ejecución para 1, 2, 4, 6 y 8 threads en el caso paralelo, con 45000000 o 90000000 (colores azul y rojo respectivamente) tiempo en ms
![execTime OMP](Graficas/tiempoOMP.png)

En este caso observamos los tiempos de ejecución de la parte de OMP con 1,2,4,6 y 8 threads con 45000000 y 90000000 operaciones como el caso anterior, tiempo en ms.

A la luz de los resultados obtenidos y representados en las gráficas podemos asegurar con total certeza que hemos llegado a un grado de optimización decente en ambas partes (paralelo y OMP), ya que en el mejor caso, desde mi maquina (8 cores), se obtiene un speedup realmente alto que en el caso del CPP contra el SEQ proporcionado es de 61.8557 (valores exactos en la hoja excel adjunta).

En cuanto a la ligera diferencia entre las optimizaciones OMP y CPP es debido a que la optimización basada en OMP es una optimización paralela general, es decir, no está exactamente optimizado para la implementación algorítmica que realizamos en esta práctica, por lo que la implementación CPP, en caso de estar correctamente realizada, siempre va a ser mínimamente más rápida, ya que esta se ha generado específicamente para este algoritmo.

Podemos ver además la clara diferencia entre usar pocos cores y usar muchos, ya que por ejemplo en la gráfica de speedups observamos como el speedUp con 4 cores es prácticamente la mitad que el speedUp con 8 cores y sucesivamente.

##### Importante: al ejecutar se han eliminado todos los prints de esta y las demás prácticas para evitar entorpecer las ejecuciones de estas, aunque al entregar se hayan vuelto a descomentar para que el profesor haga las pruebas pertinentes con toda la funcionalidad de las prácticas

##### Los criterios de ejecución de benchmarks son los mismos en los casos de las demás prácticas, por lo que no se volverá a enunciar a no ser que haya algún cambio, en cuyo caso será notificado.

##### Todos los resultados en tiempos en todas las prácticas están en milisegundos
