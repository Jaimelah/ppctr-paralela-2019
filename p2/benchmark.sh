#!/usr/bin/env bash
file=resultsFIN.log
touch $file
for season in 1 2; do
    for benchcase in par11 par12 par21 par22 par41 par42 par61 par62 par81 par82 ; do
        echo $benchcase >> $file
        for i in `seq 1 1 11`;
        do
            ./benchsuite.sh $benchcase >> $file # results dumped
            sleep 1
        done
    done
done
