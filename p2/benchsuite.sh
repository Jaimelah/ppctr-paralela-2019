#!/usr/bin/env bash
size=45000000
size2=90000000
nthreads=1
nthreads2=2
nThreads3=4
nThreads4=6
nThreads5=8
case "$1" in
    par11)
        ./build/cpp $size $nthreads
        ;;
    par12)
        ./build/cpp $size2 $nthreads
        ;;
    par21)
        ./build/cpp $size $nthreads2
        ;;
    par22)
        ./build/cpp $size2 $nthreads2
        ;;
    par41)
        ./build/cpp $size $nThreads3
        ;;
    par42)
        ./build/cpp $size2 $nThreads3
        ;;
    par61)
        ./build/cpp $size $nThreads4
        ;;
    par62)
        ./build/cpp $size2 $nThreads4
                ;;
    par81)
        ./build/cpp $size $nThreads5
        ;;
    par82)
        ./build/cpp $size2 $nThreads5
        ;;
esac
