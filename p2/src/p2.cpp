/**
*Author: Jaime López-Agudo Higuera
*P2: optimización CPP y Paralela de un programa secuencial.
**/

#include <stdio.h>
#include <math.h>
#include <cstdlib>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#ifdef SECUENCIAL
#include "p2.hpp"
#endif
#include <sys/time.h>
#ifdef PARALELO
#include <mutex>
#include <thread>
#endif
#include <iostream>
#ifdef OMP
  #include <omp.h>
#endif
#ifdef PARALELO
//Variables globales y prototipos necesarios en parte paralela(CPP)
void operaPi(double gap,int nThreads,long iterations, double *resul,int index,long opRestantes);
void parallel(double gap,long iterations, int nThreads);
std::mutex g_safe;
#endif
#ifdef OMP
//Prototipo de la fnción de openMP
void omp(double gap, long iterations, int nThreads);
#endif
//Variable de preprocesador que podemos cambiar en caso de querer que se midan los tiempos de ejecución en ms o no.
#define TIME 1
/**
* Author: Jaime López-Agudo Higuera
* @args: argv[1]=número de iteraciones argv[2]=número de threads(opcional)
*
*/
int main(int argc, char *argv[])
{
  #if TIME
    struct timeval start, end;
    double execTime=0.0;
  #endif


  #ifdef PARALELO
  //----------------------------PARALELO(CPP)----------------------
  //Paso de parámetros
  int nThreads=0;
    if(argc==3){
      nThreads=atoi(argv[2]);
    }
    if(argc==2){
      if(getenv("OMP_NUM_THREADS")){
        nThreads=atoi(getenv("OMP_NUM_THREADS"));
      }else{
        printf("variable de entorno no definida\n");
        exit(-1);
      }
    }
    if(nThreads<=0){
      printf("numero de threads no esperado\n");
      exit(-1);
    }
  #endif
  #ifdef OMP
  //----------------------------PARALELO(OMP)----------------------
  //Paso de parámetros
  int nThreads=0;
    if(argc==3){
      nThreads=atoi(argv[2]);
    }
    if(argc==2){
      if(getenv("OMP_NUM_THREADS")){
        nThreads=atoi(getenv("OMP_NUM_THREADS"));
      }else{
        printf("variable de entorno no definida\n");
        exit(-1);
      }
    }
    if(nThreads<=0){
      printf("numero de threads no esperado\n");
      exit(-1);
    }
  #endif

  long iterations=atol(argv[1]);
  //----------------------------SECUENCIAL----------------------
  #ifdef SECUENCIAL
  gettimeofday(&start,NULL);
  Data data = parse(iterations);
  sequential(data);
  gettimeofday(&end,NULL);
  execTime=(end.tv_sec-start.tv_sec)*1000+(end.tv_usec-start.tv_usec)/1000.0;
  printf("tiempo: %f ms\n",execTime);
  #endif
  #ifdef OMP
    double startO=omp_get_wtime();
    double gap= 1.0/iterations;
    omp(gap, iterations, nThreads);
    double endO=omp_get_wtime();
    printf("tiempo %f ms\n",(endO-startO)*1000);
  #endif
  #ifdef PARALELO
    gettimeofday(&start,NULL);
    double gap=1.0/iterations;
    parallel(gap, iterations,nThreads);
    gettimeofday(&end, NULL);
    execTime=(end.tv_sec-start.tv_sec)*1000+(end.tv_usec-start.tv_usec)/1000.0;
    printf("tiempo %f ms\n",execTime);
  #endif
}
#ifdef SECUENCIAL
int attach(Data* data, long iterations){
  if (iterations <= 0){
    return -1;
  }
  Aggregator* agg = (Aggregator*)malloc(sizeof(Aggregator));
  agg->separator = (double)1/(double)iterations;
  agg->A = 1.5;
  agg->B = 2.4;
  agg->C = 1.0;
  data->agg = agg;
  return 0;
}
#endif
#ifdef SECUENCIAL
Data parse(long nIts){
  // parse 'long' and set numIts
  long numIts = nIts;
  Data d;
  d.numIts = numIts;
  d.gap = 1.0/(double)d.numIts;
  d.value = 0.0;
  if (attach(&d, numIts) != 0){
    fprintf(stderr, "error: invalid iterations\n");
    exit(-1);
  };
  return d;
}
#endif
//openMP hasta la seccion 13 como minimo
#ifdef SECUENCIAL
void sequential(Data data){
	int i;
  int jump = 2.4;
	double value;
  double tmp = 0;
  double ellipse;

  int id;
  double x;
  for (i=0; i<data.numIts; i=i+1) {
    x = (i+(double)1/(int)2.0) * data.gap;
    tmp = tmp + pow(2,2)/(1+pow(x, 2));
  }
  value = tmp * data.agg->separator;
  ellipse = 2 * value / sqrt((4 * data.agg->A * data.agg->C - pow(data.agg->B, 2)));

	printf("ellipse SEQ : %f \n", ellipse);
}
#endif
#ifdef PARALELO
/**
* Author: Jaime López-Agudo Higuera
* Función llamada desde el main que se encarga del trabajo de gestión y generación de threads, además de printar el resultado obtenido
* @args: gap(double), iterations(long), nThreads (int)
*/
void parallel(double gap, long iterations,int nThreads){
  std::thread threads[nThreads];
  long opRestantes=iterations%nThreads;
  double resultado=0;
  for(auto i=0;i<nThreads-1;i++){
    threads[i]=std::thread(operaPi,gap,nThreads,iterations,&resultado,iterations/nThreads*i,0);
  }
  threads[nThreads-1]=std::thread(operaPi,gap,nThreads,iterations,&resultado,iterations/nThreads*(nThreads-1),opRestantes);
  for(auto i=0;i<nThreads;i++){
    threads[i].join();
  }
  resultado=resultado*gap;
  //Operación final para generar el área, se ha evitado añadir el sqrt de forma que se ha metido manualmente el resultado de la raiz cuadrada, ya que es
  //constante en todas las ejecuciones y para el cálculo sobre doubles, este nivel de precisión es más que suficiente
  resultado=2.0 * resultado/0.489897949;
  printf("PAR ellipse: %f \n", resultado);
}
#endif
#ifdef PARALELO
/**
* Author: Jaime López-Agudo Higuera
* Función worker de los threads generados en la función parallel que se encarga del cálculo de pi para generar más tarde el área total
* de la elipse, al final del cálculo de los threads se hace un lock_guard para poder sumar los valores sobre la variable pasada.
* @args: gap(double), nThreads(int), iterations(long), resul(double*), index(int), opRestanes(long)
*/
void operaPi(double gap,int nThreads,long iterations,double *resul, int index,long opRestantes){
  double gap2= gap*gap;
  double tmp=0.0;
  long fin=iterations/nThreads+index+opRestantes;
  for(auto i=index;i<fin;i++){
    tmp = tmp + 4/(1+((i+0.5)*(i+0.5)*gap2));
  }
  //thread ha acabado y necesitamos asegurar el acceso a la variable resul
  std::lock_guard<std::mutex>lock(g_safe);
  {
  *resul=*resul+tmp;
  }
}
#endif
#ifdef OMP
/**
* Author: Jaime López-Agudo Higuera
* Parte paralelizada mediante #pragmma omp parallel
* @args: gap(double), iterations(long), nThreads(int)
*/
void omp(double gap, long iterations, int nThreads){
  double tmp=0;
  double gap2= gap*gap;
  long i;
  //Comienza la sección paralela
  #pragma omp parallel num_threads(nThreads) private(i) reduction(+:tmp) shared(gap2,iterations)
  {
    int aux=omp_get_thread_num();
    for(i=i+aux; i<iterations;i=i+nThreads){
      tmp=tmp+4/(1+(i+0.5)*(i+0.5)*gap2);
    }
  }
  //acaba sección paralela y calculamos lo restante
  tmp=tmp*gap;
  tmp=2*tmp/0.489897949;
  printf("OMP ellipse: %f \n",tmp);
}
#endif
