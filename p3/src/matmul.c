#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/times.h>
#include <omp.h>
#define TEST 0
#define RAND rand() % 100
void init_mat_sup ();
void init_mat_inf ();
void matmul ();
void matmul_sup ();
void matmul_inf ();
void print_matrix (float *M, int dim); // TODO
void compare(float* A, float* B, int dim);

/* Usage: ./matmul <dim> [block_size]*/

int main (int argc, char* argv[])
{
	int block_size = 1, dim;
	float *A, *B, *C;
	int dim=3;
   // TODO
	float *M=(float*)malloc(dim*dim*sizeof(float));
	for(int i=0;i<dim*dim;i++){
		M[i]=i;
	}

	exit (0);
}

void init_mat_sup (int dim, float *M)
{
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j <= i)
				M[i*dim+j] = 0.0;
			else
				M[i*dim+j] = RAND;
		}
	}
}

void init_mat_inf (int dim, float *M)
{
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j >= i)
				M[i*dim+j] = 0.0;
			else
				M[i*dim+j] = RAND;
		}
	}
}
//#pragma omp parallel num_threads(OMP_NUM_THREADS) private(i,j,k) shared(C,A,B)
void matmul (float *A, float *B, float *C, int dim)
{
	double inicio=omp_get_wtime();
	int i, j, k;
	#pragma omp parallel private(i,j) shared(C,dim)
	{
		#pragma omp for schedule(static)
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;
	}
	#pragma omp parallel private(i,j,k) shared(A,B,C, dim)
	{
		#pragma omp for schedule(static)
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			for (k=0; k < dim; k++)
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];

	}

	double fin= omp_get_wtime();
	printf("El tiempo de ejecucion es %f",fin-inicio);
}

void matmul_sup (float *A, float *B, float *C, int dim)
{
	int i, j, k;
	#pragma omp parallel
	{
	#pragma omp for schedule(static)
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;
	}
	#pragma omp parallel
	{
	#pragma omp for schedule(dynamic)
	for (i=0; i < (dim-1); i++)
		for (j=0; j < (dim-1); j++)
			for (k=(i+1); k < dim; k++)
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
}

void matmul_inf (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;

	for (i=1; i < dim; i++)
		for (j=1; j < dim; j++)
			for (k=0; k < i; k++)
				C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
}
#define THRESHOLD 0.001
void compare(float* A, float* B,int dim){
	for(int i=0;i<dim*dim;i++){
		float diff=abs(A[i]-B[i]);
		if(diff > THRESHOLD){
			fprintf(stderr, "Error");
			exit(-1);

		}
	}
}
