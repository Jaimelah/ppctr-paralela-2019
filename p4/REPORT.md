# P4: Filtrado de un Vıdeo mediante Tareas OpenMP
Autor: Jaime López-Agudo Higuera
Fecha: 18-12-2019

## PREFACIO
En la práctica 4 se nos pide realizar una optimización mediante Tasks de OpenMP de un programa de filtrado de video.

# Índice

1. Sistema
2. Diseño e implementación
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

# 1. Sistema
El sistema sobre el que se han ejecutado los benchmarks es un portátil conectado a corriente alterna(para evitar posibles ahorros de energía), las características
del sistema son las siguientes:
- Host
  - CPU AMD Ryzen 5 3550H with Radeon Vega Mobile G
  - CPU 8 cores: 1 socket, 2 threads/core, 4 Cores
  - CPU @2100Mhz (fijada con cpupower en modo usuario).
  - CPU cache L1d: 128KiB, L1i: 256KiB, L2: 2MiB, L3: 4MiB
  - 8GiB RAM DDR4 3200Mhz
  - Distribución Ubuntu 19.10 actualizada.
  - CPU flags:sse, sse2, sse3, sse4a, sse4_1, sse4_2, avx...
  - Ejecutado sobre HDD 5500rpm 1 TB
- Benchmarks ejecutados sobre un procesador capado a 2.1GHz de manera manual mediante el uso de los comandos de bash cpupower, usando interfaz gráfica de la terminal y con número de procesos habituales ejecutados con el portátil en idle, es decir, el usuario no ha ejecutado ningún proceso extra durante los benchmarks ni en algún momento anterior a estos.
###### Anotación muy importante, al estar ejecutando sobre una arquitectura ZEN + (ryzen 3550H), no podemos asegurar unos speedups claros y exactos cuando ejecutamos por encima de la cantidad de cores lógicos, ya que esta microarquitectura aplica una gestión de memoria cache de instrucciones, peculiar, en la que esta caché es compartida por los 2 threads del core físico, de tal forma que al ejecutar sobre el máximo número de threads de la máquina nunca vamos a poder asegurar el speedUp correspondiente por definición(8 vs 4 threads, speedup aprox =2). Esta información esta presentada en la siguiente url https://en.wikichip.org/wiki/amd/microarchitectures/zen%2B#Memory_Hierarchy y en específico sobre mi CPU: https://en.wikichip.org/wiki/amd/ryzen_5/3550h podemos ver el mapa del die de la propia CPU y más información importante acerca de la caché y su organización. En esto también influye el infinity fabric que es un tipo de comunicación entre cores propietario de AMD, en el que la velocidad de comunicación entre cores se basa en la frecuencia de la RAM, lo cual puede tener cierta influencia en el performance dentro de los benchmarks



# 2. Diseño e implementación del software

## 2.1. Explicación del funcionamiento secuencial

En esta práctica estamos realizando un filtrado de una imagen mediante la función fgauss(), que recibe como parámetros 2 punteros a 2 buffers (arrays de ints) que guardan los pixeles de las imágenes, el primero (pixels), guarda los pixeles de la imagen sin filtrar y los vuelca una vez filtrados sobre el segundo buffer(array de ints filtered).

Para hacer esto, se crea un bucle do-while que realiza el volcado completo de los pixeles obtenidos desde el movie.in que estáran guardados en pixels[],sobre el array filtered[] una vez los pixeles hayan pasado por el método fgauss que se encarga de su filtrado.

Cada ejecución del bucle do-while consta de 3 partes principales, la primera es realizada mediante la función fread() que se encarga de la lectura de cada uno de los fotogramas que se encuentran en movie.in y coloca los pixeles en el buffer pixels[].

La segunda parte consiste en aplicar sobre el buffer pixels la función fgauss().

La tercera parte consiste en escribir en el fichero output los pixeles generados sobre el buffer filtered por la función fgauss().
Esto nos generará un archivo movie.out que contendrá la información de los pixeles ya filtrados de todos los fotogramas.

## 2.2.- Deteccion de optimización
Realizamos la ejecución de nuestro programa sobre valgrind para determinar donde se encuentra la mayoría de tiempo usado en la ejecución completa del programa.

Para usar la herramienta valgrind necesitaremos ejecutar los siguientes comandos: ```gcc -o video_task  video_task.c  -fopenmp -g (compilación) ```, ```valgrind --tool=callgrind --dump-instr=yes ./video_task```
y de esta forma se generará un ejecutable (facilitado en la carpeta gráficas de este repositorio) que mostrará lo siguiente:
![callgrind](Graficas/callgrind1.png)
Al analizar esto observamos en la parte izquierda, que es donde se muestran las distintas llamadas y el tiempo (en porcentaje) que ocupan a la CPU durante la ejecución completa del programa, de forma que observamos claramente que las 10 llamadas de la función fgauss provocan la mayor ocupación de CPU durante la ejecución, con lo que podemos determinar que la parte que necesitamos optimizar dentro del código será o la propia función fgauss o la parte del código que se encarga de las llamadas a esta.

Pero dado que se nos pide una implementación basada en tasks, optaremos por realizar una implementación en la que cada thread coja una task en cuanto pueda que contenga la parte del filtrado de la imagen, ya que es una cantidad de tarea parecida para cada task (thread), y no estamos seguros de donde están los límites de ejecución de nuestra implementación, por lo que aunque no fuese indicado en la propia práctica, las tasks serían la primera idea que deberíamos probar, ya que lo antes enunciado suele ser la situación idílica para el empleo de tasks.


## 2.3.-Optimización (OMP tasks)

La optimización mediante tasks tenemos que realizarla de forma que cada una de las tasks tome la parte de la ejecución con mayor peso temporal, es decir la parte que se encarga de la función fgauss.
La implementación queda de la siguiente manera:
```
#pragma omp parallel
{
  #pragma omp master
  {
    do
    {
      size = fread(pixels[i], (height+2) * (width+2) * sizeof(int), 1, in);
      #pragma omp task
      {
         if (size){
           fgauss (pixels[i], filtered[i], height, width);
         }
       }
       i++;
       if(i==seq-1){
         #pragma omp taskwait
         i=0;
         for(int j=0;j<seq;j++){
           fwrite(filtered[j], (height+2) * (width + 2) * sizeof(int), 1, out);
         }
       }
     } while (!feof(in));
   }
 }
```
Al principio se abre una sección paralela dentro de la que se definirán las tasks a generar en la práctica, para esto iniciamos una sección gobernada por el hilo master (```#pragma omp master ```) que se encarga de realizar las operaciones comunes con menor peso sobre la ejecución es decir, el fread() además de generar todas las tasks, que serán recogidas por los threads de nuestra CPU al quedar libres.

Dentro de esta sección libre se inicializan de manera default todas las variables a shared, por esto no se declara ninguna variable en la sección parallel, de forma que todas las variables son compartidas por los threads dentro de la sección paralela.

Después de esto se generan las tasks mediante el (```#pragma omp task ```) esto generará las tareas, que se dejarán colocadas en una cola y al quedar un thread libre, se cogerá esa task y se ejecutará como una unidad de trabajo independiente, que en nuestro caso solo ejecutará la parte del fgauss.

Al final de la ejecución del fgauss de cada una de las tareas, colocaremos un ``` #pragma omp taskwait``` que se encargue de realizar la sincronización, de forma que se espere en este punto hasta que se haya terminado de ejecutar la secuencia entera.

Una vez las tareas hayan acabado sus ejecuciones (un número seq de tareas), se hará la escritura en el archivo .out de forma ordenada mediante el uso del fwrite, se comprobará si se ha acabado (EOF) y si no se volverá a comenzar el trabajo de nuevo con otra secuencia de seq tareas, hasta que lleguemos al final del archivo, longitud de archivo que no sabemos, lo cual propicia el uso de tasks.

###### Apunte en referencia a EOF
al realizar la compilación nos va a producir un Warning, que como se nos comentó en clase es debido a que en la parte del generator.c, al meter el sfooter de la siguiente forma: (```const char sfooter[] = {'f','r','a','m','e','s',' ','c','h','u','n','k'};```)
No estamos introduciendo un carácter EOF de manera implícita, y puede llegara generar errores.

Esto no ocurre en el caso del HEADER_META(```#define HEADER_META "video custom binary format 0239" ```), que en este caso si que se coloca el carácter EOF en la cadena de caracteres.



## 2.4.-Comprobación de resultados

Para comprobar que los archivos .out generados por la parte paralela son los correctos hemos de usar el comando de bash```diff -s```, que compara 2 archivos que se pasan como argumentos e indica si estos 2 archivos son exactamente iguales o no, de esta forma nos aseguramos que la optimización de OMP ha sido correcta o necesitamos cambiar algo de la misma, ya que la parte secuencial siempre va a generar el resultado correcto.  
# 3.-Metodología y desarrollo de las pruebas realizadas
###### Seguimos la misma metodología de ejecución de benchmarks que en la práctica 2
Para el tamaño de la imagen hemos escogido el tamaño default, de 1920x1080.
Las gráficas generadas de tiempos de ejecución (ms) y speedUps de los benchmarks realizados muestran la optimización obtenida mediante las tasks de openMP, podemos observar que es un speedup cercano a 2:
![speedUP4](Graficas/speedUPP4SEQPAR.png)

Esto nos indica que hemos conseguido que la ejecucióon paralela consiga una optimización en tiempo de ejecución de la mitad de tiempo como podemos observar en la siguiente gráfica, tiempos en ms:
![execTime4](Graficas/execTimeSEQPAR.png)
