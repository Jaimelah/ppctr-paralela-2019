#!/usr/bin/env bash
file=resultsFIN.log
touch $file
for season in 1 2; do
    for benchcase in seq par; do
        echo $benchcase >> $file
        for i in `seq 1 1 22`;
        do
            ./benchsuite.sh $benchcase >> $file # results dumped
            sleep 1
        done
    done
done
