#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>
void fgauss (int *, int *, long, long);
/**
*Author: Jaime López-Agudo Higuera
*P4: programa principal que se encarga del filtrado de imagenes pixel a pixel, optimizado mediante #pragma omp tasks
*
*/
int main(int argc, char *argv[]) {
   double inicio=omp_get_wtime();
   FILE *in;
   FILE *out;
   int i, j, size, seq = 8;
   int **pixels, **filtered;
   //Paso de parámetros en caso de haberlos y control de errores
   if (argc == 2) seq = atoi (argv[1]);

//   chdir("/tmp");
   in = fopen("movie.in", "rb");
   if (in == NULL) {
      perror("movie.in");
      exit(EXIT_FAILURE);
   }

   out = fopen("movie.out", "wb");
   if (out == NULL) {
      perror("movie.out");
      exit(EXIT_FAILURE);
   }

   long width, height;
   //Lectura de tamaños de imagenes
   fread(&width, sizeof(width), 1, in);
   fread(&height, sizeof(height), 1, in);
   //Escritura de los tamaños
   fwrite(&width, sizeof(width), 1, out);
   fwrite(&height, sizeof(height), 1, out);
   //Guardado de memoria para los dos buffers para pixeles (filtrados y no fltrados)
   pixels = (int **) malloc (seq * sizeof (int *));
   filtered = (int **) malloc (seq * sizeof (int *));

   for (i=0; i<seq; i++)
   {
      pixels[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
      filtered[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
   }
   i = 0;
   //Comenzamos la sección paralela sobre la que trabajaremos
   #pragma omp parallel
   {
     //Comienza el hilo master que se encarga de crear las tareas (SOLO EJECUTA 1 HILO(master))
     #pragma omp master
     {
       do
       {
         //Sigue el master trabajando(lee sobre movie.in y vuelca pixeles sobre array(buffer))
         size = fread(pixels[i], (height+2) * (width+2) * sizeof(int), 1, in);
         //Generamos las tasks
         #pragma omp task
         {
           //TRABAJO EJEUTADO POR LAS TASKS
            if (size){
              fgauss (pixels[i], filtered[i], height, width);
            }
          }
          i++;
          if(i==seq-1){
            //barrera de sincronización de las tasks (todas esperarán en este punto hasta que la última haya acabado)
            #pragma omp taskwait
            i=0;
            for(int j=0;j<seq;j++){
              //TRABAJO DE MASTER (escribe los pixeles filtrados que se encuentran en filtered(buffer) sobre out(archivo movie.out))
              fwrite(filtered[j], (height+2) * (width + 2) * sizeof(int), 1, out);
            }
          }
        } while (!feof(in));
      }
    }
    for(int j=0;j<i-1;++j){
      //Terminamos de volcar los pixeles de filtered en caso de que quede algo que no entrase en la seq
      fwrite(filtered[j], (height+2) * (width + 2) * sizeof(int), 1, out);
    }
   for (i=0; i<seq; i++)
   {
     //Liberación de memoria(contenidos)
      free (pixels[i]);
      free (filtered[i]);
   }
   //Liberación de estructuras
   free(pixels);
   free(filtered);

   fclose(out);
   fclose(in);
   double fin=omp_get_wtime();
   printf("tiempo %f\n ms",(fin-inicio)*1000);
   return EXIT_SUCCESS;
}

void fgauss (int *pixels, int *filtered, long height, long width)
{
	int y, x, dx, dy;
	int filter[5][5] = {1, 4, 6, 4, 1, 4, 16, 26, 16, 4, 6, 26, 41, 26, 6, 4, 16, 26, 16, 4, 1, 4, 6, 4, 1};
	int sum;

	for (x = 0; x < width; x++) {
		for (y = 0; y < height; y++)
		{
			sum = 0;
			for (dx = 0; dx < 5; dx++)
				for (dy = 0; dy < 5; dy++)
					if (((x+dx-2) >= 0) && ((x+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < height))
						sum += pixels[(x+dx-2),(y+dy-2)] * filter[dx][dy];
			filtered[x*height+y] = (int) sum/273;
		}
	}
}
