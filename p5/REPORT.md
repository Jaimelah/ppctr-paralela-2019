# P5: Fractal de Mandelbrot
Autor: Jaime López-Agudo Higuera
Fecha: 11-12-2019

## PREFACIO
La práctica 5 consiste en un grupo de ejercicios que tienen como objetivo determinar las diferencias en diversas implementaciones de openMP para la optimización de un programa que genera una imagen de fractales con diferentes precisiones en función de los parámetros que le pasamos.

# Índice

1. Sistema
2. Diseño e implementación
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

# 1. Sistema
El sistema sobre el que se han ejecutado los benchmarks es un portátil conectado a corriente alterna(para evitar posibles ahorros de energía), las características
del sistema son las siguientes:
- Host
  - CPU AMD Ryzen 5 3550H with Radeon Vega Mobile G
  - CPU 8 cores: 1 socket, 2 threads/core, 4 Cores
  - CPU @2100Mhz (fijada con cpupower en modo usuario).
  - CPU cache L1d: 128KiB, L1i: 256KiB, L2: 2MiB, L3: 4MiB
  - 8GiB RAM DDR4 3200Mhz
  - Distribución Ubuntu 19.10 actualizada.
  - CPU flags:sse, sse2, sse3, sse4a, sse4_1, sse4_2, avx...
  - Ejecutado sobre HDD 5500rpm 1 TB
- Benchmarks ejecutados sobre un procesador capado a 2.1GHz de manera manual mediante el uso de los comandos de bash cpupower, usando interfaz gráfica de la terminal y con número de procesos habituales ejecutados con el portátil en idle, es decir, el usuario no ha ejecutado ningún proceso extra durante los benchmarks ni en algún momento anterior a estos.

###### Anotación muy importante, al estar ejecutando sobre una arquitectura ZEN + (ryzen 3550H), no podemos asegurar unos speedups claros y exactos cuando ejecutamos por encima de la cantidad de cores lógicos, ya que esta microarquitectura aplica una gestión de memoria cache de instrucciones, peculiar, en la que esta caché es compartida por los 2 threads del core físico, de tal forma que al ejecutar sobre el máximo número de threads de la máquina nunca vamos a poder asegurar el speedUp correspondiente por definición(8 vs 4 threads, speedup aprox =2). Esta información esta presentada en la siguiente url https://en.wikichip.org/wiki/amd/microarchitectures/zen%2B#Memory_Hierarchy y en específico sobre mi CPU: https://en.wikichip.org/wiki/amd/ryzen_5/3550h podemos ver el mapa del die de la propia CPU y más información importante acerca de la caché y su organización. En esto también influye el infinity fabric que es un tipo de comunicación entre cores propietario de AMD, en el que la velocidad de comunicación entre cores se basa en la frecuencia de la RAM, lo cual puede tener cierta influencia en el performance dentro de los benchmarks


# 2. Diseño e implementación del software

## 2.1. Ejercicio 1 Deteccion de optimización
En el primer ejercicio se nos pide realizar un análisis de rendimiento del algoritmo secuencial proporcionado en la práctica, en este caso usaremos la herramienta valgrind para generar la información, aunque antes de hacer nada bajaremos el número n (tamaño de imagen) desde el valor 8001 a 4001 para acabar la compilación en un tiempo asequible.

para poder usar la herramienta necesitamos ejecutar  los siguientes comandos: ```gcc -o p5  p5.c  -fopenmp -lm -g (compilación) ``` , ```valgrind --tool=callgrind --dump-instr=yes ./p5 1.ppm```, esto nos generará un archivo que más tarde podremos ejecutar mediante el comando ```kcachegrind callgrind.out.9448```, al ejecutarlo observamos lo siguiente:
![callgrind](Graficas/callgrindP5.png)

En la parte izquierda del problema vemos que nos marca que la función explode ha sido llamada un total de 16008001 veces durante la ejecución del programa, además, las llamadas a esta función ocupan un 74.08% del tiempo de ejecución total del mismo.

Esto nos indica claramente que nuestro candidato a la optimización va a ser la parte del código que realiza todas las llamadas a la función explode().

La siguiente parte será observar la funcionalidad del código y determinar que partes de este se encargan de hacer todas las llamadas a la función explode para determinar que paralelización con openMP debemos aplicar (tasks, parallel, parallel for...).

En el código proporcionado, la única llamada a la función explode se da en el siguiente segmento:

```
count = (int *)malloc(n * n * sizeof(int));

for (i = 0; i < n; i++) {
  for (j = 0; j < n; j++) {
    x = ((double)(j)*x_max + (double)(n - j - 1) * x_min) / (double)(n - 1);

    y = ((double)(i)*y_max + (double)(n - i - 1) * y_min) / (double)(n - 1);

    count[i + j * n] = explode(x, y, count_max);
  }
}
```
En este segmento de código tenemos 2 bucles for anidados que se encargan  de hacer las llamadas a la función, por lo que hay muchas posibilidades que la mayor optimización en openMP la obtengamos haciendo uso de ```#pragma omp for ``` sobre estos bucles con algún tipo de scheduler (static, dynamic o guided).

También podemos observar que parte del tiempo de ejecución se va en la parte en la que se realizan raíces cuadradas, por lo que esta parte también será paralelizada como extra.

## 2.2. Ejercicio 2 Optimización OMP
En el apartado anterior ya hemos determinado cual va a ser la parte que vamos a optimizar, con lo que solo queda realizar la optimización en openMP, para esto vamos a hacer uso de la directiva ```#pragma omp for ```, para esto primero hemos de delimitar una región paralela sobre la que se va a realizar la subregión for que va a generar el paralelismo.

Dentro de la región paralela tenemos que determinar que variables van a ser privada y cuales van a ser compartidas(la región paralela usada es la segunda seguún el código(```   #pragma omp parallel num_threads(omp_get_max_threads()) private(x,y,i,j,c), shared(count,count_max,r,g,b,n,x_max,y_max,c_max, x_min,y_min) ```)):
- Variables privadas: las variables privadas van a ser las variables internas de cada bucle, en este caso i y j y x e y además de c, que es usada como extra al paralelizar la parte de las raíces cuadradas.
En este caso hacemos estas variable privadas porque son variables que necesitamos de forma independiente en cada uno de los bucles internos de cada uno de los threads generados.
- Variables compartidas: en este caso serán count_max (precisión de los fractales , constante), n (tamaño de la imagen en pixeles, constante), x_max, y_max x_min e y min (rango constante de números de x e y) y count(array sobre el que se escribe en distintas posiciones el resultado de la función explode).
Además introducimos como extra para las raices cuadradas c_max y c_min
Hacemos compartidas estas variables ya que o son constantes, como en el caso de las primeras o es un array que accedemos a posiciones distintas a lo largo del array, por lo que no necesitamos protegerlas.
También será compartida c_max será usada en el apartado 4 y necesita ser protegida de distintas maneras, dando origen a distintas implementaciones.
- También se podía haber definido el default(none), para que en caso de encontrar una variable no definida en la región paralela, salte un error de compilación y para forzarnos a indicar explícitamente los tipos de variables, en caso de no haberlo definido, todas las variables será shared.

Dentro de esta región paralela tendemos que definir la región ```#pragma omp for ``` que se encargará de la optimización del bucle, tenemos que determinar que sheduler vamos a utilizar, esta elección la elegiremos en función de como se distribuyen las cargas de trabajo dentro del bucle for, que en este caso podemos afirmar con seguridad que son cargas distintas, ya que dentro de explode() tenemos un bucle for con un if dentro en el que si se cumple la condición salimos completamente de la ejecución del bucle, lo cual nos indica que no todas las ejecuciones van a ser iguales, por lo que la elección más acertada de scheduler es o dynamic o guided, que en este caso, tras numerosas pruebas con ambos y distintos tamaños de bloque hemos determinado que el punto crítico más optimo para los valores que tenemos es un scheduler dinámico con un tamaño de bloque de alrededor de 10, ya que si lo hacemos muy grande, esto ira progresivamente acercándose a la funcionalidad de un scheduler estático y si lo hacemos muy pequeño usaremos demasiado tiempo realizando cambios de contexto desde un thread a otro.

##### Importante
Este valor ha sido probado desde mi máquina con un tamaño de imagen de de 8001x8001 con una precisión de 124000, con lo que no se asegura la optimalidad del código con este valor sobre otras máquinas con otras configuraciones de precisión y de tamaño de imagen.

Por lo que el código ya optimizado queda de la siguiente forma:
```
#ifdef MODE4
//Al usar reduction con max, no necesitamos proteger c_max
  #pragma omp parallel num_threads(omp_get_max_threads()) private(x,y,i,j,c), shared(count,count_max,r,g,b,n,x_max,y_max)
#else
      #pragma omp parallel num_threads(omp_get_max_threads()) private(x,y,i,j,c), shared(count,count_max,r,g,b,n,x_max,y_max,c_max, x_min,y_min)
#endif
{
  //optimización del ej2 ejecución del explode()
  #pragma omp for schedule(dynamic,10)
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      x = ((double)(j)*x_max + (double)(n - j - 1) * x_min) / (double)(n - 1);
      y = ((double)(i)*y_max + (double)(n - i - 1) * y_min) / (double)(n - 1);
      count[i + j * n] = explode(x, y, count_max);
    }
  }
```

## 2.3. Ejercicio 3

En el ejercicio 3 se nos pide 4 implementaciones distintas de la sección crítica que determina el coloreado de los pixeles.

- La primera implementación que se pide es generar una implementación que, basándose en directivas de openMP se encargue de la protección de la variable c_max, para esto hemos optado por el uso de una región paralela externa con un scheduler estático, ya que las cargas de trabajo de cada uno de los threads son muy similares, por lo que no tendría demasiado sentido implementarlo de otra manera,
además de una región crítica que se encarga de realizar las operaciones que envuelven la variable c_max, de esta forma, protegeremos el acceso a la misma.
```
#ifdef MODE1
  #pragma omp for schedule(static)
  for (j = 0; j < n; j++) {
    for (i = 0; i < n; i++) {
      #pragma omp critical
      {
        if (c_max < count[i + j * n]) {
            c_max = count[i + j * n];
        }
      }
    }
  }
#endif
```

Esta implementación como veremos en las gráficas generadas es realmente muy poco eficiente en cuanto a tiempos de ejecución, ya que todas las operaciones tienen que ser realizadas en la región critical, de forma que solo podrá estar ejecutando esa sección un thread durante todo el tiempo de ejecución.

- En la segunda parte se pide una implementación con la misma funcionalidad, pero esta vez usando funciones del runtime de openMP, con o que deberemos usar los locks que nos proporciona openMP (omp_lock_t) y el código queda asi, es decir implementar la protección sobre la variable c_max usando mutex, lo cual es una implementación bastante parecida a la de la práctica 1 y 2, pero en OMP, ya que en esos casos necesitábamos proteger el acceso a la variable resultado calculada por los threads de manera independiente:
```
#ifdef MODE2
  #pragma omp for schedule(static)
  for (j = 0; j < n; j++) {
    for (i = 0; i < n; i++) {
      omp_set_lock(&lock);
      if (c_max < count[i + j * n]) {
            c_max = count[i + j * n];
      }
      omp_unset_lock(&lock);
    }
  }
```
En este fragmento observamos como se genera el lock en la línea superior y como se inicializa mediante la función omp_init_lock(). El lock le iniciaremos justo antes de entrar a la sección crítica del programa (omp_set_lock) y lo quitamos al salir de la sección crítica: omp_unset_lock(). EL lock es destruido al final de la sección paralela, al final de la ejecución completa de todos los modos, para esto necesitaremos la función omp_destroy_lock().

Importante: se necesita incluir la librería <omp.h> ya que si no no tendremos acceso a las funciones de runtime de openMP.

Esta parte igual que la anterior es realmente muy poco eficiente, ya que estamos realizando todas las operaciones en una sección protegida por mutexes, lo cual va a provocar que solo 1 thread pueda estar dentro de dicha sección a a cada momento, con lo que la implementación es realmente similar a la del modo anterior.

- En la tercera parte se pide una implementación secuencial, para ello hemos usado la directiva ```#pragma omp single``` que nos indica que la zona determinada por esta directiva solo será ejecutada por 1 hilo, también podíamos haber usado el ```#pragma omp master``` que tiene una funcionalidad parecida, ya que solo ejecuta esta zona 1 hilo, pero en este caso es el hilo master el que se encarga de ello. Pero se ha usado ```#pragma omp single``` como podemos observar en el código:
```   
#ifdef MODE3
  #pragma omp single
  {
    for (j = 0; j < n; j++) {
      for (i = 0; i < n; i++) {

        if (c_max < count[i + j * n]) {
          c_max = count[i + j * n];
          }
        }
      }
  }
#endif
```
Por lo que los tiempos de ejecución de esta implementación han de ser similares, pero un poco mayores que los de la parte secuencial sola, ya que estamos introduciendo cierta cantidad de overhead mediante el uso de esta directiva de openMP.

- En la cuarta parte se nos pide realizar la implementación de tal manera que usemos variables privadas de cada thread y se seleccione el máximo de todas ellas.
Para esto usaremos la clause reduction del omp for, de forma que se guarde en la variable c_max el número máximo siempre de los valores calculados, de forma que se vea más compacto el código generado, ambas implementaciones tienen unos tiempos de ejecución parecidos por lo que es indiferente el uso de cualquiera de las dos
Implementación actual:
```
#ifdef MODE4
  #pragma omp for schedule(static) reduction(max:c_max)
  for (j = 0; j < n; j++) {
    for (i = 0; i < n; i++) {
        if (c_max < count[i + j * n]) {
            c_max = count[i + j * n];
        }
    }
  }
#endif
```
Implementación pasada:

```
#ifdef MODE4
double max = 0;
  #pragma omp for schedule(dynamic, 10)
  for (j = 0; j < n; j++) {
    for (i = 0; i < n; i++) {
      if (max < count[i + j * n]) {
        max = count[i + j * n];
      }
    }
  }
  #pragma omp critical
  {
    if (c_max < max) {
      c_max = max;
    }
  }

#endif
```
En este caso, la primera implementación es más optima, ya que nos ahorramos el uso de una sección crítica debido a que usamos la clause reduction(max:c_max), que, como ya hemos indicado antes, funciona guardando siempre el valor máximo en la variable indicada (c_max en este caso)

# 2.4. Makefile

Se ha generado un Makefile que se encarga de generar todos los ejecutables de manera independiente y de forma conjunta:
```
parallel:
	mkdir -p build
	gcc -o build/parallel src/p5.c -D PARALLEL -fopenmp -lm
mode1:
	mkdir -p build
	gcc -o build/mode1  src/p5.c -D MODE1 -fopenmp -lm
mode2:
	mkdir -p build
	gcc -o build/mode2 src/p5.c -D MODE2 -fopenmp -lm
mode3:
	mkdir -p build
	gcc -o build/mode3 src/p5.c -D MODE3 -fopenmp -lm
mode4:
	mkdir -p build
	gcc -o build/mode4 sc/p5.c -D MODE4 -fopenmp -lm
all:
	mkdir -p build
	gcc -o build/parallel src/p5.c -D PARALLEL -fopenmp -lm
	gcc -o build/mode1  src/p5.c -D MODE1 -fopenmp -lm
	gcc -o build/mode2 src/p5.c -D MODE2 -fopenmp -lm
	gcc -o build/mode3 src/p5.c -D MODE3 -fopenmp -lm
	gcc -o build/mode4 src/p5.c -D MODE4 -fopenmp -lm
clear:
	rm -rf build

```
De forma que nos facilite la ejecución de los benchmarks al tener ya generados los ejecutables necesarios.
# 3.-Metodología y desarrollo de las pruebas realizadas

##### Mantenemos criterios de ejecución desde la práctica 2 pero usamos el siguiente benchmark:

```
#!/usr/bin/env bash
file1=mode1.log
file2=mode2.log
file3=mode3.log
file4=mode4.log
file5=seq.log
file6=par.log
touch $file1
touch $file2
touch $file3
touch $file4
touch $file5
touch $file6
for season in 1 2; do
    for benchcase in seq; do
        echo $benchcase >> $file5
        for i in `seq 1 1 11`;
        do
            ./benchsuite.sh $benchcase >> $file5 # results dumped
            sleep 1
        done
    done
    for benchcase in mode1; do
        echo $benchcase >> $file1
        for i in `seq 1 1 11`;
        do
            ./benchsuite.sh $benchcase >> $file1 # results dumped
            sleep 1
        done
    done
    for benchcase in mode2; do
        echo $benchcase >> $file2
        for i in `seq 1 1 11`;
        do
            ./benchsuite.sh $benchcase >> $file2 # results dumped
            sleep 1
        done
    done
    for benchcase in mode3; do
        echo $benchcase >> $file3
        for i in `seq 1 1 11`;
        do
            ./benchsuite.sh $benchcase >> $file3 # results dumped
            sleep 1
        done
    done
    for benchcase in mode4; do
        echo $benchcase >> $file4
        for i in `seq 1 1 11`;
        do
            ./benchsuite.sh $benchcase >> $file4 # results dumped
            sleep 1
        done
    done
    for benchcase in par; do
        echo $benchcase >> $file6
        for i in `seq 1 1 11`;
        do
            ./benchsuite.sh $benchcase >> $file6# results dumped
            sleep 1
        done
    done
done

```

benchsuite.sh:

```

#!/usr/bin/env bash

case "$1" in
    mode1)
        ./build/mode1
        ;;
    mode2)
      ./build/mode2
        ;;
    mode3)
        ./build/mode3
        ;;
    mode4)
        ./build/mode4
        ;;
     seq)
        ./build/seq
esac

```

Importante, la parte ejecutada en todos los casos es el ROI, es decir lo que hemos optimizado en cada uno de los casos de la actividad 3, más la parte paralela de la actividad 2 como extra de ejecución, para tomar los tiempos hemos usado gettimeofday() de la librería time.h, todos los resultados de tiempos de ejecución del ROI se muestran en ms y obtenemos la siguiente gráfica de tiempo de ejecución:
![execTime5](Graficas/EXEC_TIMEP5.png)

En cuanto a los speedUps generados vemos los siguientes resultados: ![speedUps5](Graficas/speedUPSP5.png)

En este caso podemos observar que él único modo que obtiene una mejora tangible en relación a la parte secuencial es el modo4, (Ej3-4), en el que realizamos una implementación basada en la clausula reduction para obtener los resultados mayores, en vez de tener que hacer sincronizaciones a cada vuelta del bucle, lo cual ahorra gran cantidad de tiempo dentro del pragma omp for.
