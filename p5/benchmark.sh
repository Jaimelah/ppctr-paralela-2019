#!/usr/bin/env bash
file1=mode1.log
file2=mode2.log
file3=mode3.log
file4=mode4.log
file5=seq.log
file6=par.log
touch $file1
touch $file2
touch $file3
touch $file4
touch $file5
touch $file6
for season in 1 2; do
    for benchcase in seq; do
        echo $benchcase >> $file5
        for i in `seq 1 1 11`;
        do
            ./benchsuite.sh $benchcase >> $file5 # results dumped
            sleep 1
        done
    done
    for benchcase in mode1; do
        echo $benchcase >> $file1
        for i in `seq 1 1 11`;
        do
            ./benchsuite.sh $benchcase >> $file1 # results dumped
            sleep 1
        done
    done
    for benchcase in mode2; do
        echo $benchcase >> $file2
        for i in `seq 1 1 11`;
        do
            ./benchsuite.sh $benchcase >> $file2 # results dumped
            sleep 1
        done
    done
    for benchcase in mode3; do
        echo $benchcase >> $file3
        for i in `seq 1 1 11`;
        do
            ./benchsuite.sh $benchcase >> $file3 # results dumped
            sleep 1
        done
    done
    for benchcase in mode4; do
        echo $benchcase >> $file4
        for i in `seq 1 1 11`;
        do
            ./benchsuite.sh $benchcase >> $file4 # results dumped
            sleep 1
        done
    done
    for benchcase in par; do
        echo $benchcase >> $file6
        for i in `seq 1 1 11`;
        do
            ./benchsuite.sh $benchcase >> $file6# results dumped
            sleep 1
        done
    done
done
