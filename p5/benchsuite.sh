#!/usr/bin/env bash

case "$1" in
    mode1)
        ./build/mode1
        ;;
    mode2)
      ./build/mode2
        ;;
    mode3)
        ./build/mode3
        ;;
    mode4)
        ./build/mode4
        ;;
     seq)
        ./build/seq
        ;;
     par)
        ./build/parallel
        ;;
esac
